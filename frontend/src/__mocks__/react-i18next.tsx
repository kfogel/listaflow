import React from "react";
import { TransProps } from "react-i18next";

const reactI18Next: any = jest.createMockFromModule("react-i18next");

declare global {
  interface Window {
    TEXT_DIRECTION: "ltr" | "rtl";
  }
}

window.TEXT_DIRECTION = "ltr";

reactI18Next.useTranslation = () => {
  return {
    t: (str: string) => str,
    i18n: {
      changeLanguage: () => new Promise(() => {}),
      dir: () => window.TEXT_DIRECTION,
    },
  };
};

reactI18Next.Trans = (prop: TransProps<any>) => (
  <React.Fragment>{prop.children}</React.Fragment>
);

module.exports = reactI18Next;

export default {};
