import { fireEvent, render, screen } from "../../utils/test-utils";
import { ChecklistDetail } from "../ChecklistDetail";

let mockReadyVariable: Boolean = true;
let mockErrors: { messages?: string[] } = { messages: ["this is an error"] };

const topLevelTasks = [
  {
    x: {
      id: 1,
      label: "delectus aut autem",
      body: "body 1",
      completed: false,
      required: true,
      interface_type: "checkbox",
    },
    p: { response: {} },
  },
  {
    x: {
      id: 2,
      label: "quis ut nam facilis et officia qui",
      body: "body 2",
      completed: true,
      interface_type: "checkbox",
    },
    p: { response: {} },
  },
  {
    x: {
      id: 3,
      label: "subsection",
      interface_type: "subsection",
      children: [],
    },
    p: { response: {} },
  },
];

const subtasks = [
  {
    x: {
      id: 4,
      label: "subtask",
      interface_type: "checkbox",
      parent: 3,
    },
    p: { response: {} },
  },
];

// mock useList from providence
jest.mock("@opencraft/providence/react-plugin", () => {
  return {
    useList: (name: string[]) => {
      return {
        getOnce: () => {},
        get: () => {
          return { catch: () => {} };
        },
        setPage: (pageNum: number) => {
          pageNum;
        },
        errors: mockErrors,
        ready: mockReadyVariable,
        list: name.length === 2 ? topLevelTasks : subtasks,
        makeReady: () => {},
      };
    },
    useSingle: () => {
      return {
        getOnce: () => {},
        get: () => {
          return { then: () => {}, catch: () => {} };
        },
        errors: mockErrors,
        ready: mockReadyVariable,
        p: { completed: {}, num: {}, val: {} },
        x: {
          name: "some name",
          body: "some body",
          num: 0,
        },
      };
    },
  };
});

describe("ChecklistDetail page", () => {
  it("renders providence list items", async () => {
    mockReadyVariable = true;
    render(<ChecklistDetail />);
    const checklist1 = screen.getByText(/delectus aut autem/i);
    expect(checklist1).toBeInTheDocument();

    const checklist2 = screen.getByText(/quis ut nam facilis et officia qui/i);
    expect(checklist2).toBeInTheDocument();
  });

  it("renders providence errors", async () => {
    mockReadyVariable = false;
    render(<ChecklistDetail />);
    const checklist1 = screen.getByText(/This is an error/i);
    expect(checklist1).toBeInTheDocument();
  });

  it("renders subsections expanded", async () => {
    // Arrange
    mockReadyVariable = true;
    render(<ChecklistDetail />);
    // Assert
    expect(screen.getByText("subsection")).toBeInTheDocument();
    const subtask = screen.getByText("subtask");
    expect(subtask).toBeInTheDocument();
    const subtaskAccordionCollapse = subtask.closest(".accordion-collapse");
    expect(subtaskAccordionCollapse).not.toBeNull();
    expect(subtaskAccordionCollapse!.className).toBe(
      "accordion-collapse collapse show"
    );
  });

  it("renders pending notice", async () => {
    mockReadyVariable = true;
    mockErrors = {};
    render(<ChecklistDetail />);
    const pendingTaskNotice = screen.getByText(
      /checklistDetail.form.pendingTasks/i
    );
    expect(pendingTaskNotice).toBeInTheDocument();
  });

  it("renders force submit alert on submit", async () => {
    mockReadyVariable = true;
    mockErrors = {};
    render(<ChecklistDetail />);
    const btn = screen.getByText(/checklistDetail.form.submitBtn/i);
    expect(btn).toBeInTheDocument();
    const pendingTaskNotice = screen.getByText(
      /checklistDetail.form.pendingTasks/i
    );
    expect(pendingTaskNotice).toBeInTheDocument();
    fireEvent.click(btn);
    // shows alert and hides btn and other text around it.
    const alert = screen.getByText(/checklistDetail.form.forceSaveAlertTitle/i);
    expect(alert).toBeInTheDocument();
    expect(btn).not.toBeInTheDocument();
    expect(pendingTaskNotice).not.toBeInTheDocument();
  });
});
