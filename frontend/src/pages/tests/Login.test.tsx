import { useTranslation } from "react-i18next";
import { fireEvent, render, screen, waitFor } from "../../utils/test-utils";
import Login from "../Login";

const { t } = useTranslation();

jest.mock("../../constants/api-urls", () => ({
  TOKEN_PATH: "#",
  GOOGLE_CONVERT_TOKEN_PATH: "#",
}));

test("test login form", async () => {
  const { queryAllByText, queryByText } = render(<Login />);

  expect(queryAllByText(/`${t("login.form.title")}`/i)).toBeTruthy();

  fireEvent.change(screen.getByLabelText(t("login.form.email")), {
    target: { value: "e@ecom" },
  });
  fireEvent.blur(screen.getByLabelText(t("login.form.email")));
  await waitFor(() => {
    expect(
      queryByText(/Emails without a full domain name are not supported./i)
    ).toBeTruthy();
  });

  fireEvent.change(screen.getByLabelText(t("login.form.email")), {
    target: { value: "123sadadf23zsdas" },
  });
  fireEvent.change(screen.getByLabelText(t("login.form.email")), {
    target: { value: "e@e.com" },
  });
  await waitFor(() => {
    expect(
      queryByText(/Emails without a full domain name are not supported./i)
    ).toBeNull();
  });
});

test("test google button hidden", async () => {
  const { queryByText } = render(<Login />);
  expect(queryByText(t("loginWithGoogle"))).toBeNull();
});

test("test visible google button", async () => {
  process.env.REACT_APP_GOOGLE_CLIENT_ID = "fake-id";
  const { queryByText } = render(<Login />);

  expect(queryByText(t("loginWithGoogle"))).toBeTruthy();
});
