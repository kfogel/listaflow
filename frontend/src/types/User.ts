export declare interface User {
  id: number;
  username: string;
  email: string;
}

export declare interface DisplayUser {
  username: string;
  display_name?: string;
}

export declare interface Assignee extends DisplayUser {
  completed: boolean;
}

export declare interface Team {
  id: string;
  name: string;
  description: string;
  members: User[];
}

export declare interface TeamNames {
  id: string;
  name: string;
}

export declare interface Token {
  access_token?: string;
  refresh_token?: string;
}

export declare interface AuthData extends Token {
  username?: string;
  email?: string;
  displayName?: string;
  password?: string;
  id?: number;
}

export declare interface UserSession extends AuthData {
  rememberMe?: boolean;
}

export declare interface UserFormData {
  username: string;
  password: string;
  email?: string;
  id?: number;
  display_name?: string;
  rememberMe?: boolean;
}

export declare interface LoginMetaData {
  client_id: string;
  client_secret: string;
  grant_type?: string;
  refresh_token?: string;
  access_token?: string;
  token?: string;
  backend?: string;
  redirect_uri?: string;
}

export declare interface LoginPostData extends UserFormData, LoginMetaData {}
