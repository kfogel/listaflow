import { ListController } from "@opencraft/providence/base/lists/types/ListController";
import Select, { MultiValue } from "react-select";

interface MultiSelectDropdownArgs {
  name: string;
  onChange: (newValue: MultiValue<{ value?: string; label?: string }>) => void;
  selectedValue: Array<string>;
  controller: ListController<any>;
}

export const MultiSelectDropdown = ({
  name,
  onChange,
  selectedValue,
  controller,
}: MultiSelectDropdownArgs) => {
  const selectedRows = controller.list.filter((item) =>
    item.x?.id ? selectedValue.includes(item.x?.id) : false
  );
  const defaultValue = selectedRows.map((item) => {
    return {
      value: item?.x?.id,
      label: item?.x?.name,
    };
  });

  return (
    <Select
      className="react-select-container"
      classNamePrefix="react-select"
      isMulti
      onChange={onChange}
      value={selectedValue ? defaultValue : undefined}
      isLoading={controller.fetching}
      isClearable={true}
      isSearchable={true}
      name={name}
      options={controller.list.map((item) => {
        return { value: item.x?.id, label: item.x?.name };
      })}
    />
  );
};

export default MultiSelectDropdown;
