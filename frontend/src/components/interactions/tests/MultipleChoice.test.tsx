import { useSingle } from "@opencraft/providence/react-plugin";
import { fireEvent, render, screen } from "../../../utils/test-utils";
import { InterfaceComponentProps } from "../../../types/Interaction";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { MultipleChoice } from "../MultipleChoice";

let testController: SingleController<InterfaceComponentProps>;
const available_choices = ["A", "B", "C", "D", "E"];

const TestComponent = () => {
  testController = useSingle<InterfaceComponentProps>("testController", {
    endpoint: "#",
    x: {
      customizationArgs: {
        minimum_selected: 1,
        maximum_selected: 3,
        available_choices,
      },
      response: null,
    },
  });

  return (
    <MultipleChoice
      customizationArgs={testController.x!.customizationArgs}
      response={testController.p.response}
    />
  );
};

const TestComponentWithNoMax = () => {
  testController = useSingle<InterfaceComponentProps>("testController", {
    endpoint: "#",
    x: {
      customizationArgs: {
        minimum_selected: 1,
        available_choices,
      },
      response: null,
    },
  });

  return (
    <MultipleChoice
      customizationArgs={testController.x!.customizationArgs}
      response={testController.p.response}
    />
  );
};

describe("MultipleChoice component with minimum and maximum limit", () => {
  beforeEach(() => {
    render(<TestComponent />);
  });

  it("should render options correctly", async () => {
    for (const item of available_choices) {
      const input = screen.getByText(item);
      expect(input).toBeInTheDocument();
      expect(input.className).toContain("btn-pill-check");
    }
    const selectAll = screen.queryByText("multiChoice.select_all");
    expect(selectAll).not.toBeInTheDocument();
  });

  it("should render select label correctly", async () => {
    const label = screen.getByText("multiChoice.minmax");
    expect(label).toBeInTheDocument();
  });

  it("should set response correctly", async () => {
    const A = screen.getByText("A");
    fireEvent.click(A);
    expect(testController.p.response.model).toStrictEqual({
      selected_choices: ["A"],
    });

    const D = screen.getByText("D");
    fireEvent.click(D);
    expect(testController.p.response.model).toStrictEqual({
      selected_choices: ["A", "D"],
    });
  });
});

describe("MultipleChoice component with only minimum limit", () => {
  beforeEach(() => {
    render(<TestComponentWithNoMax />);
  });

  it("should render options correctly", async () => {
    for (const item of available_choices) {
      const input = screen.getByText(item);
      expect(input).toBeInTheDocument();
      expect(input.className).toContain("btn-pill-check");
    }
    const selectAll = screen.getByText("multiChoice.select_all");
    expect(selectAll).toBeInTheDocument();
  });

  it("should render select label correctly", async () => {
    const label = screen.getByText("multiChoice.minimumOnly");
    expect(label).toBeInTheDocument();
  });

  it("should set response correctly", async () => {
    const A = screen.getByText("A");
    fireEvent.click(A);
    expect(testController.p.response.model).toStrictEqual({
      selected_choices: ["A"],
    });

    const D = screen.getByText("D");
    fireEvent.click(D);
    expect(testController.p.response.model).toStrictEqual({
      selected_choices: ["A", "D"],
    });

    const selectAll = screen.getByText("multiChoice.select_all");
    fireEvent.click(selectAll);
    expect(testController.p.response.model).toStrictEqual({
      selected_choices: available_choices,
    });
  });
});
