import { useSingle } from "@opencraft/providence/react-plugin";
import { fireEvent, render, screen } from "../../../utils/test-utils";
import { NumericInput } from "../NumericInput";
import { InterfaceComponentProps } from "../../../types/Interaction";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";

let testController: SingleController<InterfaceComponentProps>;

const TestComponentWithStep = () => {
  testController = useSingle<InterfaceComponentProps>("testController", {
    endpoint: "#",
    x: {
      customizationArgs: {
        min_value: 0,
        max_value: 10,
        step: 2,
      },
      response: null,
    },
  });

  return (
    <NumericInput
      customizationArgs={testController.x!.customizationArgs}
      response={testController.p.response}
    />
  );
};

const TestComponentWithoutStep = () => {
  testController = useSingle<InterfaceComponentProps>("testController2", {
    endpoint: "#",
    x: {
      customizationArgs: {
        min_value: 1,
        max_value: 5,
        step: null,
      },
      response: null,
    },
  });

  return (
    <NumericInput
      customizationArgs={testController.x!.customizationArgs}
      response={testController.p.response}
    />
  );
};

describe("NumericInput component with step value 1", () => {
  beforeEach(() => {
    render(<TestComponentWithStep />);
  });

  it("should render numeric input correctly", async () => {
    const input = screen.getByPlaceholderText("0");
    expect(input).toBeInTheDocument();
    expect(input.className).toContain("form-control");
  });

  it("should render increment & decrement button", async () => {
    const decrement = screen.getByText("-");
    expect(decrement).toBeInTheDocument();
    expect(decrement.className).toContain("btn-numeric");

    const increment = screen.getByText("+");
    expect(increment).toBeInTheDocument();
    expect(increment.className).toContain("btn-numeric");
  });

  it("should increment & decrement value on button click", async () => {
    const decrement = screen.getByText("-");
    const increment = screen.getByText("+");
    // The input doesn't have any value, it sets to minimum on first click.
    fireEvent.click(increment);
    expect(testController.p.response.model).toStrictEqual({ number: 0 });
    // Second click is required to check the functionality.
    fireEvent.click(increment);
    expect(testController.p.response.model).toStrictEqual({ number: 2 });

    fireEvent.click(decrement);
    expect(testController.p.response.model).toStrictEqual({ number: 0 });
  });
});

describe("NumericInput component with step value 0", () => {
  beforeEach(() => {
    render(<TestComponentWithoutStep />);
  });

  it("should render numeric input correctly", async () => {
    const input = screen.getByPlaceholderText("0");
    expect(input).toBeInTheDocument();
    expect(input.className).toContain("form-control");
  });

  it("should render increment & decrement button", async () => {
    const decrement = screen.getByText("-");
    expect(decrement).toBeInTheDocument();
    expect(decrement.className).toContain("btn-numeric");

    const increment = screen.getByText("+");
    expect(increment).toBeInTheDocument();
    expect(increment.className).toContain("btn-numeric");
  });

  it("should increment & decrement value on button click", async () => {
    const decrement = screen.getByText("-");
    const increment = screen.getByText("+");
    // The input doesn't have any value, it sets to minimum on first click.
    fireEvent.click(increment);
    expect(testController.p.response.model).toStrictEqual({ number: 1 });
    // Second click is required to check the functionality.
    fireEvent.click(increment);
    expect(testController.p.response.model).toStrictEqual({ number: 2 });

    fireEvent.click(decrement);
    expect(testController.p.response.model).toStrictEqual({ number: 1 });
  });
});
