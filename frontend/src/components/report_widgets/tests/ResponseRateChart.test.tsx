import { render, screen } from "../../../utils/test-utils";
import ResponseRateChart from "../ResponseRateChart";
import { ResponseRate } from "../../../types/Report";

const dummyResponseRates = () => [
  {
    completed_count: 4,
    total_count: 10,
    timestamp: "2022-11-12T08:58:35.852525Z",
  },
  {
    completed_count: 1,
    total_count: 10,
    timestamp: "2022-11-11T08:58:37.869231Z",
  },
  {
    completed_count: 0,
    total_count: 10,
    timestamp: "2022-11-10T08:58:39.919188Z",
  },
  {
    completed_count: 0,
    total_count: 10,
    timestamp: "2022-11-09T08:58:41.948484Z",
  },
];

const Container = (props: { rates?: ResponseRate[] }) => {
  return (
    <div data-testid="beep">
      <ResponseRateChart {...props} />
    </div>
  );
};

describe("Bar chart", () => {
  it("Should render a bar chart left to right", () => {
    window.TEXT_DIRECTION = "ltr";
    render(<Container rates={dummyResponseRates()} />);
    expect(screen.getByTestId("beep").textContent).toContain(
      "Nov 9, 2022Nov 10, 2022Nov 11, 2022Nov 12, 2022"
    );
  });
  it("Should render a bar chart right to left", () => {
    window.TEXT_DIRECTION = "rtl";
    render(<Container rates={dummyResponseRates()} />);
    expect(screen.getByTestId("beep").textContent).toContain(
      "Nov 12, 2022Nov 11, 2022Nov 10, 2022Nov 9, 2022"
    );
  });
});
