"""Initialization for the conf module.

Celery needs to be loaded with the workflow module so that task registration and
discovery can work correctly.
"""
# This will make sure the app is always imported when Django starts so
# that shared_task will use this app, and also ensures that the celery
# singleton is always configured for the CMS.
from .celery_app import APP as CELERY_APP

__all__ = ["CELERY_APP"]
