"""helpers for sending email"""

from datetime import date
from typing import Literal, Optional
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import engines, TemplateSyntaxError
from django.utils import timezone

from workflow.models import Email


def _get_run_context(run_instance):
    """
    Return run related details like end_date, due_date, etc.
    """
    context = {
        "end_date": run_instance.end_date.date(),
        "due_date": run_instance.due_date,
        "start_date": run_instance.start_date.date(),
    }

    def _get_date_details(run_date: date, key_prefix=""):
        """
        Returns details of date like day of week, month, date etc.
        """
        context = {
            f"{key_prefix}month_name": run_date.strftime("%b"),
            f"{key_prefix}day_of_month": str(run_date.day),
            f"{key_prefix}day_of_week": run_date.strftime("%A"),
            f"{key_prefix}year": str(run_date.year),
        }
        return context

    context.update(_get_date_details(run_instance.end_date, key_prefix="run_end_"))
    context.update(_get_date_details(run_instance.due_date, key_prefix="due_date_"))
    context.update(_get_date_details(run_instance.start_date, key_prefix="run_start_"))
    return context


def template_from_string(template_string, using=None):
    """
    Convert a string into a template object,
    using a given template engine or using the default backends
    from settings.TEMPLATES if no engine was specified.
    """
    # This function is based on django.template.loader.get_template,
    # but uses Engine.from_string instead of Engine.get_template.
    chain = []
    engine_list = engines.all() if using is None else [engines[using]]
    for engine in engine_list:
        try:
            return engine.from_string(template_string)
        except TemplateSyntaxError as error:
            chain.append(error)
    raise TemplateSyntaxError(chain)


def _get_templates_or_default(
    email_type: Literal["reminder", "notification"],
    email_instance: Optional[Email] = None,
):
    """
    gets a template object for given recurrence_instance if present from
    database else uses default templates
    """
    html_body_template = get_template(f"email/checklist_{email_type}/body.html")
    text_body_template = get_template(f"email/checklist_{email_type}/body.txt")
    subject_template = get_template(f"email/checklist_{email_type}/subject.txt")
    if email_instance:
        if email_instance.html_body:
            html_body_template = template_from_string(email_instance.html_body)
        if email_instance.text_body:
            text_body_template = template_from_string(email_instance.text_body)
        if email_instance.subject:
            subject_template = template_from_string(email_instance.subject)
    return subject_template, text_body_template, html_body_template


def _get_rendered_email_message(
    context: dict,
    email_type: Literal["reminder", "notification"],
    email_instance: Optional[Email] = None,
):
    """
    Returns rendered email message from db if dynamic email template for given
    recurrence exists else returns default templates Also, takes care of
    selecting between notification and reminder templates based on due_date
    """
    (
        subject_template,
        text_body_template,
        html_body_template,
    ) = _get_templates_or_default(email_type, email_instance)
    email_subject = subject_template.render(context=context)
    email_text_body = text_body_template.render(context=context)
    email_html_body = html_body_template.render(context=context)
    reply_to = (
        email_instance.reply_to if email_instance else settings.DEFAULT_REPLY_TO_EMAILS
    )

    return email_subject, email_text_body, email_html_body, reply_to


# pylint: disable=too-many-arguments
def _send_pending_checklist_notification_mails(
    email: str,
    checklist_context: dict,
    run_context: dict,
    conn,
    email_type: Literal["reminder", "notification"],
    email_instance: Optional[Email] = None,
):
    """Sends pending checklist notification to the given email.

    Args:
        email: str. The email of the user.
        checklist_context: dict. Should contain display_name, checklist_name,
        checklist_id and task_list
        run_context: dict. Should contain start_date, end_date & due_date
        recurrence_instance: Recurrence. recurrence instance of the run
        conn: email backend connection
    """
    context = {"site_url": settings.FRONTEND_URL}
    context.update(run_context)
    context.update(checklist_context)
    (
        email_subject,
        email_text_body,
        email_html_body,
        reply_to,
    ) = _get_rendered_email_message(context, email_type, email_instance)
    mail_obj = EmailMultiAlternatives(
        email_subject.strip(),
        email_text_body,
        settings.DEFAULT_FROM_EMAIL,
        [email],
        reply_to=reply_to,
        connection=conn,
    )
    mail_obj.attach_alternative(email_html_body, "text/html")
    mail_obj.send()


def send_pending_checklist_email_for_runs(run_instance, conn, days_pending):
    """Sends pedning checklist notification to the assignee.

    Args:
        run_instance: Instance of Run model.
        conn: email backend conn
        days_pending: timedetla between due_date and today
    """
    run_context = _get_run_context(run_instance)
    for checklist_instance in run_instance.checklists.filter(completed=False):
        checklist_context = {
            "display_name": checklist_instance.assignee.display_name,
            "checklist_name": checklist_instance.name,
            "checklist_id": checklist_instance.id,
            "task_list": checklist_instance.task_names_list().all(),
        }
        email = checklist_instance.assignee.email
        if days_pending > timezone.timedelta(1):
            email_type = "notification"
            email_instance = run_instance.recurrence.notification_email
        else:
            email_type = "reminder"
            email_instance = run_instance.recurrence.reminder_email
        _send_pending_checklist_notification_mails(
            email,
            checklist_context,
            run_context,
            conn=conn,
            email_type=email_type,
            email_instance=email_instance,
        )
