"""
Workflow - Recurrence Trends Report - Recomputing trigger command
"""

import sys

from django.core.management.base import BaseCommand

from workflow.models import ChecklistDefinition, Recurrence, RunTrendTaskReport
from workflow.tasks import compute_task_definition_trends_report


class Command(BaseCommand):
    """Create (if not exist) and compute trends report for recurrence"""

    def add_arguments(self, parser):
        """
        Create and return the ``ArgumentParser`` which will be used to
        parse the arguments to this command.
        """
        parser.add_argument(
            "--checklist-definition-id",
            default=None,
            help="Checklist definition ID to trigger computing for",
            required=True,
        )

    def handle(self, *args, **options):
        """Handle command logic"""
        checklist_definition_id = options["checklist_definition_id"]
        processed: list[tuple[str, bool, str]] = []

        try:
            processed = self.create_and_compute_trends_report(checklist_definition_id)
        except ChecklistDefinition.DoesNotExist:
            self.stderr.write(
                f"Recurrence with id {checklist_definition_id} doesn't exist"
            )
            sys.exit(1)

        self.stdout.write(f"Processed {len(processed)} in total")
        for definition, created, run_id in processed:
            action = "created_and_computed" if created else "computed"
            self.stdout.write(f"ACTION=[{action}], run=[{run_id}], task=[{definition}]")

    @staticmethod
    def create_and_compute_trends_report(
        checklist_definition_id: str,
    ) -> list[tuple[str, bool, str]]:
        """Trigger creating and computing trends report

        Args:
            recurrence_id: str. ID of target Recurrence.
        Raises:
            RecurrenceDoesNotExist: Recurrence with given ID
            doesn't exist.
        """
        processed: list[tuple[str, bool, str]] = []
        checklist_definition = ChecklistDefinition.objects.get(
            pk=checklist_definition_id
        )
        task_definitions = checklist_definition.task_definitions.all()
        runs = checklist_definition.related_runs().all()
        for run in runs:
            for task_definition in task_definitions:
                _, created = RunTrendTaskReport.objects.get_or_create(
                    run=run, task_definition=task_definition
                )
                compute_task_definition_trends_report.apply_async(
                    args=[task_definition.id, run.id]
                )
                processed.append((task_definition.label, created, run.id))

        return processed
