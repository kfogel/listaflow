"""
Handle caching and CSV writing logic for reports
"""
from typing import Optional

from utils.cache import make_cache_key, make_dict_cache_key, make_pk_cache_key


class CacheModelReportBase:
    """
    Base for reports based on models
    """

    instance = None

    def get_instance(self):
        """
        Get model instance
        """
        return self.instance

    @classmethod
    def make_cache_prefix(cls):
        """
        Report cache prefix, default is empty
        """
        return ""

    @classmethod
    def make_cache_suffix(cls):
        """
        Report cache suffix, default is empty
        """
        return ""

    @classmethod
    def context_key(cls, context: Optional[dict] = None):
        """
        make key from context
        """
        if not context:
            return ""
        keys = (
            val for val in context.values() if isinstance(val, (str, int, bool, list))
        )
        return make_cache_key(*keys)

    @classmethod
    def make_cache_key(cls, instance, context: Optional[dict] = None):
        """
        Make cache key for report based on model instance
        """
        prefix = cls.make_cache_prefix()
        if isinstance(instance, dict):
            model_pk_cache_key = make_dict_cache_key(instance)
        else:
            model_pk_cache_key = make_pk_cache_key(instance)
        suffix = cls.make_cache_suffix()
        context_key = cls.context_key(context)

        return make_cache_key(prefix, model_pk_cache_key, suffix, context_key)
