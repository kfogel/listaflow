"""
Django app configuration for workflow module.
"""
from django.apps import AppConfig


class WorkflowConfig(AppConfig):
    """
    Workflow module configuration object.
    """

    # We will actually be using ShortCodeField for primary keys, but you have to
    # subclass AutoField for that even though, presumably, the whole point of AutoField
    # was to support non-integer keys. In any case, it's better to specify
    # explicitly since it would be a surprise to have an implicit shortcode field,
    # especially to people new to the project, since this is not a frequently seen
    # feature yet.
    default_auto_field = "django.db.models.BigAutoField"
    name = "workflow"

    def ready(self):
        # pylint: disable=import-outside-toplevel disable=unused-import
        import workflow.signals

        assert workflow.signals
