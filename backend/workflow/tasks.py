"""Celery tasks for the app."""
from datetime import date
from typing import Optional, Union, List
from celery import shared_task
from django.core import mail
from django.db import transaction
from django.utils import timezone
from django.utils.dateparse import parse_datetime

from utils.cache import clear_cache, clear_cache_multi
from workflow.helpers.email import send_pending_checklist_email_for_runs
from workflow.models import (
    ChecklistTaskDefinition,
    Recurrence,
    Run,
    RunTrendTaskReport,
)


@shared_task(name="workflow.tasks.create_checklist_for_users")
def create_checklist_for_users(recurrence_id: str):
    """Task to create checklists and send emails to the users.

    Args:
        recurrence_id: str. The Id of the Recurrence model.
    """
    recurrence_instance = Recurrence.objects.get(id=recurrence_id)
    with transaction.atomic():
        recurrence_instance.create_run()


def coerce_to_date(date_candidate: Union[str, bytes, date]) -> date:
    """
    Coerces a string into a date if it is not one, or else returns
    the original object, assuming it to be a date.
    """
    if type(date_candidate) in (str, bytes):
        return parse_datetime(date_candidate)
    return date_candidate


@shared_task
def create_run_for_team(
    recurrence_id: Optional[str] = None,
    participants: Optional[List[str]] = None,
    start_date: Optional[Union[str, bytes, date]] = None,
    end_date: Optional[Union[str, bytes, date]] = None,
    due_date: Optional[Union[str, bytes, date]] = None,
):  # pylint: disable=too-many-arguments
    """Wraps Run.create_run_for_team in a celery task.

    Args:
        recurrence_id: Recurrence id. This is passed in case a
          run is created from Recurrence instance.
        participants: A list of email addresses corresponding to
          the team members to include. Emails which aren't
          associated with any team member will be ignored.
        start_date: Optional, run start date.
        end_date: Optional, run end date.
        due_date: Optional, run due date.
    """
    recurrence_instance = Recurrence.objects.get(id=recurrence_id)
    dates = {
        "start_date": start_date,
        "end_date": end_date,
        "due_date": due_date,
    }
    dates = {
        key: coerce_to_date(date_string)
        for key, date_string in dates.items()
        if date_string
    }
    with transaction.atomic():
        recurrence_instance.create_run(participants=participants, **dates)


@shared_task
def compute_task_definition_trends_report(task_definition_id: str, run_id: str = None):
    """Task to compute report every time a ChecklistTask is saved

    Args:
        task_definition_id: str. The ID of the TaskDefinition instance.
        recurrence_id: str. Recurrence ID, compute all recurrences if not passed
    """
    task_definition = ChecklistTaskDefinition.objects.get(id=task_definition_id)
    run = Run.objects.filter(id=run_id).first()
    with transaction.atomic():
        RunTrendTaskReport.compute_task_trends(task_definition, run)


@shared_task
def flush_cache(cache_key: str):
    """
    Task to clear cache for given cache key
    """
    clear_cache(cache_key)


@shared_task(name="workflow.tasks.send_reminder_emails")
def send_reminder_emails(today_str: Optional[str] = None):
    """
    Task to send reminder mail to each assignee.
    """

    today = timezone.now().date()
    if today_str:
        today = timezone.datetime.strptime(today_str, "%Y-%m-%d").date()

    def _should_send_mail(due_on, reminder_offsets):
        for reminder_days in reminder_offsets:
            reminder_date = due_on - timezone.timedelta(days=reminder_days)
            if reminder_date == today:
                return True
        return False

    valid_runs = Run.objects.filter(due_date__gte=today, recurrence__isnull=False)
    with mail.get_connection() as conn:
        for valid_run in valid_runs:
            due_date = valid_run.due_date
            reminders = valid_run.reminders
            if _should_send_mail(due_date, reminders):
                days_pending = due_date - today
                send_pending_checklist_email_for_runs(valid_run, conn, days_pending)


@shared_task
def flush_many_cache(cache_key_regex: str):
    """
    Clears multiple cache for given regex.
    """
    clear_cache_multi(cache_key_regex)
