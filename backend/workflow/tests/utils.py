"""
Utility functions for test
"""
from typing import List

from profiles.tests.factories import TeamFactory, UserFactory
from workflow.interaction import (
    CHECKBOX_INTERFACE,
    get_interfaces_customization_arg_default_value,
)
from workflow.tests.factories import (
    ChecklistDefinitionFactory,
    ChecklistTaskDefinitionFactory,
)


def create_team_and_checklist_definition(
    *,
    username_prefix: str = "default",
    team_member_count: int = 2,
    tasks_kwargs: List[dict] = None,
):
    """A helper method to create a checklist_definition and a team model."""
    tasks_kwargs = [{}] if tasks_kwargs is None else tasks_kwargs
    users: list = []
    for index in range(team_member_count):
        username = f"{username_prefix}_user_{index}"
        users.append(UserFactory(username=username))
    team = TeamFactory()
    team.members.set(users)
    checklist_definition = ChecklistDefinitionFactory()
    tasks = []
    for task in tasks_kwargs:
        tasks.append(
            ChecklistTaskDefinitionFactory(
                checklist_definition=checklist_definition,
                **task,
            )
        )

    return team, checklist_definition, tasks


def create_checklist_task_definition(
    checklist_definition: ChecklistDefinitionFactory,
    interface_type: str = CHECKBOX_INTERFACE,
):
    """A helper method to create a checklist task definition"""
    default_customization_args = get_interfaces_customization_arg_default_value()
    task_definition = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        interface_type=interface_type,
        customization_args=default_customization_args[interface_type],
    )
    return task_definition
