#
# This file is autogenerated by pip-compile with Python 3.10
# by the following command:
#
#    pip-compile --output-file=requirements.txt requirements/constraints.in requirements/requirements.in
#
amqp==5.1.1
    # via kombu
argon2-cffi==21.3.0
    # via django
argon2-cffi-bindings==21.2.0
    # via argon2-cffi
asgiref==3.4.1
    # via django
astroid==2.9.0
    # via pylint
asttokens==2.0.5
    # via stack-data
async-timeout==4.0.2
    # via redis
attrs==21.2.0
    # via
    #   jsonschema
    #   pytest
backcall==0.2.0
    # via ipython
billiard==3.6.4.0
    # via celery
black==21.12b0
    # via -r requirements/requirements.in
boto3==1.23.9
    # via django-storages
botocore==1.26.9
    # via
    #   boto3
    #   s3transfer
cachetools==5.3.0
    # via google-auth
celery==5.2.6
    # via
    #   -r requirements/requirements.in
    #   django-celery-beat
    #   django-celery-email
    #   flower
certifi==2021.10.8
    # via
    #   requests
    #   sentry-sdk
cffi==1.15.0
    # via
    #   argon2-cffi-bindings
    #   cryptography
charset-normalizer==2.0.9
    # via requests
click==8.0.3
    # via
    #   black
    #   celery
    #   click-didyoumean
    #   click-plugins
    #   click-repl
click-didyoumean==0.3.0
    # via celery
click-plugins==1.1.1
    # via celery
click-repl==0.2.0
    # via celery
collectfast==2.2.0
    # via -r requirements/requirements.in
coreapi==2.3.3
    # via
    #   djangorestframework-stubs
    #   drf-yasg
coreschema==0.0.4
    # via
    #   coreapi
    #   drf-yasg
coverage[toml]==6.2
    # via pytest-cov
cron-descriptor==1.2.31
    # via -r requirements/requirements.in
cryptography==37.0.1
    # via
    #   jwcrypto
    #   python-jose
    #   social-auth-core
cycler==0.11.0
    # via matplotlib
decorator==5.1.1
    # via ipython
defusedxml==0.7.1
    # via
    #   python3-openid
    #   social-auth-core
deprecated==1.2.13
    # via
    #   jwcrypto
    #   redis
django[argon2]==3.2.13
    # via
    #   -r requirements/requirements.in
    #   collectfast
    #   django-anymail
    #   django-appconf
    #   django-celery-beat
    #   django-celery-email
    #   django-cors-headers
    #   django-extensions
    #   django-filter
    #   django-oauth-toolkit
    #   django-redis
    #   django-rest-registration
    #   django-storages
    #   django-stubs
    #   django-stubs-ext
    #   django-taggit
    #   django-timezone-field
    #   djangorestframework
    #   djangorestframework-bulk
    #   drf-nested-routers
    #   drf-yasg
    #   rest-condition
django-anymail==8.6
    # via -r requirements/requirements.in
django-appconf==1.0.5
    # via django-celery-email
django-better-admin-arrayfield==1.4.2
    # via -r requirements/requirements.in
django-celery-beat==2.2.1
    # via -r requirements/requirements.in
django-celery-email==3.0.0
    # via -r requirements/requirements.in
django-cors-headers==3.12.0
    # via -r requirements/requirements.in
django-environ==0.8.1
    # via -r requirements/requirements.in
django-extensions==3.1.5
    # via -r requirements/requirements.in
django-filter==22.1
    # via -r requirements/requirements.in
django-oauth-toolkit==2.0.0
    # via drf-social-oauth2
django-redis==5.2.0
    # via -r requirements/requirements.in
django-rest-framework==0.1.0
    # via -r requirements/requirements.in
django-rest-registration==0.7.2
    # via -r requirements/requirements.in
django-storages[boto3]==1.12.3
    # via
    #   -r requirements/requirements.in
    #   collectfast
django-stubs==1.9.0
    # via
    #   -r requirements/requirements.in
    #   djangorestframework-stubs
django-stubs-ext==0.3.1
    # via django-stubs
django-taggit==3.0.0
    # via -r requirements/requirements.in
django-timezone-field==4.2.3
    # via django-celery-beat
djangorestframework==3.13.0
    # via
    #   django-rest-framework
    #   django-rest-registration
    #   djangorestframework-bulk
    #   drf-nested-routers
    #   drf-social-oauth2
    #   drf-yasg
    #   rest-condition
djangorestframework-bulk==0.2.1
    # via -r requirements/requirements.in
djangorestframework-stubs==1.4.0
    # via -r requirements/requirements.in
drf-nested-routers==0.93.4
    # via -r requirements/requirements.in
drf-social-oauth2==1.2.1
    # via -r requirements/requirements.in
drf-yasg==1.20.0
    # via -r requirements/requirements.in
ecdsa==0.17.0
    # via python-jose
execnet==1.9.0
    # via pytest-xdist
executing==0.8.3
    # via stack-data
factory-boy==3.2.1
    # via -r requirements/requirements.in
faker==10.0.0
    # via
    #   -r requirements/requirements.in
    #   factory-boy
flower==1.0.0
    # via -r requirements/requirements.in
fonttools==4.34.4
    # via matplotlib
google-auth==2.16.0
    # via google-auth-oauthlib
google-auth-oauthlib==1.0.0
    # via -r requirements/requirements.in
gunicorn==20.1.0
    # via -r requirements/requirements.in
humanize==4.1.0
    # via flower
idna==3.3
    # via requests
inflection==0.5.1
    # via drf-yasg
iniconfig==1.1.1
    # via pytest
ipython==8.4.0
    # via -r requirements/requirements.in
isort==5.10.1
    # via pylint
itypes==1.2.0
    # via coreapi
jedi==0.18.1
    # via ipython
jinja2==3.0.3
    # via coreschema
jmespath==1.0.0
    # via
    #   boto3
    #   botocore
jsonschema==4.5.1
    # via -r requirements/requirements.in
jwcrypto==1.2
    # via django-oauth-toolkit
kiwisolver==1.4.4
    # via matplotlib
kombu==5.2.4
    # via celery
lazy-object-proxy==1.7.1
    # via
    #   -r requirements/constraints.in
    #   astroid
markupsafe==2.0.1
    # via jinja2
matplotlib==3.5.2
    # via -r requirements/requirements.in
matplotlib-inline==0.1.3
    # via ipython
mccabe==0.6.1
    # via pylint
mypy==0.920
    # via
    #   django-stubs
    #   djangorestframework-stubs
mypy-extensions==0.4.3
    # via
    #   black
    #   mypy
numpy==1.23.1
    # via matplotlib
oauthlib==3.2.0
    # via
    #   django-oauth-toolkit
    #   requests-oauthlib
    #   social-auth-core
packaging==21.3
    # via
    #   drf-yasg
    #   matplotlib
    #   pytest
    #   redis
parso==0.8.3
    # via jedi
pathspec==0.9.0
    # via black
pexpect==4.8.0
    # via ipython
pickleshare==0.7.5
    # via ipython
pillow==9.2.0
    # via
    #   matplotlib
    #   reportlab
platformdirs==2.4.0
    # via
    #   black
    #   pylint
pluggy==1.0.0
    # via pytest
prometheus-client==0.14.1
    # via flower
prompt-toolkit==3.0.29
    # via
    #   click-repl
    #   ipython
psycopg2==2.9.2
    # via -r requirements/requirements.in
ptyprocess==0.7.0
    # via pexpect
pure-eval==0.2.2
    # via stack-data
py==1.11.0
    # via
    #   pytest
    #   pytest-forked
pyasn1==0.4.8
    # via
    #   pyasn1-modules
    #   python-jose
    #   rsa
pyasn1-modules==0.2.8
    # via google-auth
pycparser==2.21
    # via cffi
pygments==2.12.0
    # via ipython
pyjwt==2.3.0
    # via social-auth-core
pylint==2.12.2
    # via
    #   -r requirements/requirements.in
    #   pylint-django
    #   pylint-plugin-utils
pylint-django==2.4.4
    # via -r requirements/requirements.in
pylint-plugin-utils==0.6
    # via pylint-django
pyparsing==3.0.6
    # via
    #   matplotlib
    #   packaging
pyrsistent==0.18.1
    # via jsonschema
pytest==6.2.5
    # via
    #   -r requirements/requirements.in
    #   pytest-cov
    #   pytest-django
    #   pytest-env
    #   pytest-forked
    #   pytest-xdist
pytest-cov==3.0.0
    # via -r requirements/requirements.in
pytest-django==4.5.2
    # via -r requirements/requirements.in
pytest-env==0.6.2
    # via -r requirements/requirements.in
pytest-forked==1.4.0
    # via pytest-xdist
pytest-xdist==2.5.0
    # via -r requirements/requirements.in
python-crontab==2.6.0
    # via django-celery-beat
python-dateutil==2.8.2
    # via
    #   botocore
    #   faker
    #   matplotlib
    #   python-crontab
python-jose[cryptography]==3.3.0
    # via drf-social-oauth2
python3-openid==3.2.0
    # via social-auth-core
pytz==2021.3
    # via
    #   celery
    #   django
    #   django-timezone-field
    #   djangorestframework
    #   flower
redis==4.3.1
    # via
    #   -r requirements/requirements.in
    #   django-redis
reportlab==3.6.10
    # via -r requirements/requirements.in
requests==2.26.0
    # via
    #   coreapi
    #   django-anymail
    #   django-oauth-toolkit
    #   djangorestframework-stubs
    #   requests-oauthlib
    #   social-auth-core
requests-oauthlib==1.3.1
    # via
    #   google-auth-oauthlib
    #   social-auth-core
rest-condition==1.0.3
    # via -r requirements/requirements.in
rsa==4.8
    # via
    #   google-auth
    #   python-jose
ruamel-yaml==0.17.17
    # via drf-yasg
s3transfer==0.5.2
    # via boto3
sentry-sdk==1.5.12
    # via -r requirements/requirements.in
short-stuff==1.0.2
    # via -r requirements/requirements.in
six==1.16.0
    # via
    #   asttokens
    #   click-repl
    #   ecdsa
    #   google-auth
    #   python-dateutil
social-auth-app-django==5.0.0
    # via drf-social-oauth2
social-auth-core==4.2.0
    # via social-auth-app-django
sqlparse==0.4.2
    # via django
stack-data==0.2.0
    # via ipython
text-unidecode==1.3
    # via faker
toml==0.10.2
    # via
    #   django-stubs
    #   pylint
    #   pytest
tomli==1.2.3
    # via
    #   black
    #   coverage
    #   mypy
tornado==6.1
    # via flower
traitlets==5.2.1.post0
    # via
    #   ipython
    #   matplotlib-inline
types-pytz==2021.3.3
    # via django-stubs
types-pyyaml==6.0.1
    # via django-stubs
typing-extensions==4.0.1
    # via
    #   black
    #   collectfast
    #   django-stubs
    #   django-stubs-ext
    #   djangorestframework-stubs
    #   mypy
uritemplate==4.1.1
    # via
    #   coreapi
    #   drf-yasg
urllib3==1.26.7
    # via
    #   botocore
    #   requests
    #   sentry-sdk
vine==5.0.0
    # via
    #   amqp
    #   celery
    #   kombu
wcwidth==0.2.5
    # via prompt-toolkit
wrapt==1.13.3
    # via
    #   astroid
    #   deprecated

# The following packages are considered to be unsafe in a requirements file:
# setuptools
