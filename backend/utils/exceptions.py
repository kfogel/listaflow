"""
exception helpers
"""
from typing import Optional
from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework.exceptions import ValidationError


def raise_django_validation_error(
    key: str, *msgs: str, error: Optional[Exception] = None
):
    """
    wrapper to raise django ValidationError
    """
    if error:
        raise DjangoValidationError({key: list(msgs)}) from error
    raise DjangoValidationError({key: list(msgs)})


def raise_rest_validation_error(
    key: str, *msgs: str, error: Optional[Exception] = None
):
    """
    wrapper to raise rest_framework ValidationError
    """
    if error:
        raise ValidationError({key: list(msgs)}) from error
    raise ValidationError({key: list(msgs)})
