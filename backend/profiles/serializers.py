"""
Serializers for anything which affects everything.
"""

from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_registration.api.serializers import DefaultRegisterUserSerializer
from rest_registration.api.views.register import VerifyRegistrationSerializer

from profiles.models import Team


# pylint: disable=W0223
class ResendEmailVerificationSerializer(serializers.Serializer):
    """
    serializer for resend email verification view
    """

    id = serializers.IntegerField(required=True)


class CustomVerifyRegistrationSerializer(VerifyRegistrationSerializer):
    """
    custom verify serializer to include client_id and client_secret
    """

    client_id = serializers.CharField()
    client_secret = serializers.CharField()


class RegisterSerializer(DefaultRegisterUserSerializer):
    """
    override default register serializer from rest-registration
    """

    class Meta:
        """
        declare meta data
        """

        model = get_user_model()
        fields = ("username", "password", "email", "display_name")
        extra_kwargs = {
            "display_name": {"required": False},
            "password": {"write_only": True},
        }

    # pylint: disable=R0201
    def validate_email(self, email: str) -> str:
        """
        validate email field, allow users from whitelisted domains only
        """

        allowed_domains = [
            domain.lower()
            for domain in getattr(settings, "DEFAULT_WHITELISTED_DOMAINS", [])
        ]
        domain = email.split("@", 1)[1]
        if domain.lower() not in allowed_domains:
            raise serializers.ValidationError("Invalid domain name")
        return email

    # pylint: disable=R0201
    def create(self, validated_data):
        """
        create user with display name
        """

        user = get_user_model().objects.create_user(
            username=validated_data["username"],
            email=validated_data["email"],
            password=validated_data["password"],
        )
        user.is_active = False
        display_name = validated_data.get("display_name")
        if display_name:
            user.display_name = display_name
        user.save()
        return user


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for User
    """

    class Meta:
        model = get_user_model()
        fields = (
            "id",
            "email",
            "username",
            "display_name",
        )
        read_only_fields = fields


class TeamSerializer(serializers.ModelSerializer):
    """
    Serializer for Team
    """

    members = UserSerializer(many=True)

    class Meta:
        model = Team
        fields = (
            "id",
            "name",
            "description",
            "members",
        )
        read_only_fields = fields


class TeamNameSerializer(serializers.ModelSerializer):
    """
    Serializer for Team
    """

    class Meta:
        model = Team
        fields = (
            "id",
            "name",
        )
        read_only_fields = fields
