"""
Tests for profile module.
"""
import pytest

from profiles.serializers import RegisterSerializer

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_display_name_on_create():
    """
    test whether display name is setup
    """
    user_data = {
        "username": "username",
        "password": "1sdc23zdsaf",
        "email": "e@heroes.com",
        "display_name": "hero",
    }
    serializer = RegisterSerializer(data=user_data)
    assert serializer.is_valid()
    instance = serializer.save()
    assert instance.display_name == "hero"


def test_domain_validation():
    """
    test whether display name is setup
    """
    user_data = {
        "username": "username",
        "password": "1sdc23zdsaf",
        "email": "e@heroes.com",
        "display_name": "hero",
    }
    serializer = RegisterSerializer(data=user_data)
    assert serializer.is_valid()
    user_data["email"] = "e@villians.com"
    serializer = RegisterSerializer(data=user_data)
    assert not serializer.is_valid()
