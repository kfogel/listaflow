"""
management commands for profile app
"""
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Add display names to all users
    """

    help = (
        "Shortcut to fill empty display names using first and last name if available"
        "if first_name and last_name are missing, username is used."
    )

    def handle(self, *args, **options):
        """
        updates all users missing display_name
        """
        user_table = get_user_model()
        users = user_table.objects.filter(display_name=None).all()
        for user in users:
            user.update_display_name()
            user.save()
